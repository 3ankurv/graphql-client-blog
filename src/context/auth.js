import React, { createContext, useReducer } from "react";
import jwtDecode from "jwt-decode";

const initialState = {
  user: null,
};

if (window.localStorage.getItem("jwtToken")) {
  const decodedToken = jwtDecode(window.localStorage.getItem("jwtToken"));
  if (decodedToken.exp * 1000 < Date.now()) {
    window.localStorage.removeItem("jwtToken");
  } else {
    initialState.user = decodedToken;
  }
}
const AuthContext = createContext({
  user: null,
  login: (userData) => {},
  logout: () => {},
});

function authReducer(state, action) {
  switch (action.type) {
    case "LOGIN":
      return {
        ...state,
        user: action.payload,
      };
      break;
    case "LOGOUT":
      return {
        ...state,
        user: null,
      };
    default:
      return state;
  }
}

function AuthProvider(props) {
  const [state, dispath] = useReducer(authReducer, initialState);

  function login(userData) {
    window.localStorage.setItem("jwtToken", userData.token);
    dispath({
      type: "LOGIN",
      payload: userData,
    });
  }

  function logout() {
    window.localStorage.removeItem("jwtToken");
    dispath({
      type: "LOGOUT",
    });
  }

  return (
    <AuthContext.Provider
      value={{ user: state.user, login, logout }}
      {...props}
    />
  );
}

export { AuthContext, AuthProvider };
