import React, { useContext, useState, useEffect } from "react";
import {
  Grid,
  Image,
  Card,
  Button,
  Label,
  Icon,
  Form,
  TextArea,
} from "semantic-ui-react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import moment from "moment";
import { AuthContext } from "../context/auth";
import LikeButton from "../components/LikeButton";
import CreateComment from "../components/CreateComment";
import { GET_POST_DETAILS } from "../util/graphql";

function SinglePost(props) {
  //  console.log(props);

  const { user } = useContext(AuthContext);
  const [postData, setPostData] = useState({});
  const { postId } = props.match.params;
  const { loading, data } = useQuery(GET_POST_DETAILS, {
    onError(error) {},
    variables: { postId },
  });

  // useEffect(()=>{

  // },[])

  // useEffect(()=>{

  // }, [postData])

  console.log("is post updating", data);

  const { getPost: post } = data || {};
  const {
    username = null,
    createdAt = null,
    body = null,
    commentCount,
    likeCount = 0,
    comments = [],
  } = post || {};
  console.log("Post details", post);
  return (
    <div>
      <div>
        {post && username && (
          <Grid>
            <Grid.Row>
              <Grid.Column width={2}>
                <Image
                  floated="right"
                  size="large"
                  src="https://react.semantic-ui.com/images/avatar/large/matthew.png"
                />
              </Grid.Column>
              <Grid.Column width={10}>
                <Card fluid>
                  <Card.Content extra>
                    <Card.Header>{username}</Card.Header>
                    <Card.Meta>{moment(createdAt).fromNow()}</Card.Meta>
                    <Card.Description>{body}</Card.Description>
                  </Card.Content>

                  <Card.Content extra>
                    <LikeButton post={post} user={user} />

                    <Button
                      as="div"
                      labelPosition="right"
                      onClick={() => {}}
                      basic
                    >
                      <Button color="blue">
                        <Icon name="comments" />
                      </Button>
                      <Label basic color="blue" pointing="left">
                        {commentCount}
                      </Label>
                    </Button>

                    {user && user.username === username && (
                      <Button
                        as="div"
                        color="red"
                        onClick={() => {}}
                        floated="right"
                      >
                        <Icon name="trash" />
                      </Button>
                    )}
                  </Card.Content>
                </Card>

                <CreateComment {...props} />

                {comments.map((postComment) => {
                  return (
                    <Card fluid key={postComment.id}>
                      <Card.Content extra>
                        {user && user.username === postComment.username && (
                          <Button
                            as="div"
                            color="red"
                            onClick={() => {}}
                            floated="right"
                          >
                            <Icon name="trash" />
                          </Button>
                        )}
                        <Card.Header>{postComment.username}</Card.Header>
                        <Card.Meta>
                          {moment(postComment.createdAt).fromNow()}
                        </Card.Meta>
                        <Card.Description>{postComment.body}</Card.Description>
                      </Card.Content>
                    </Card>
                  );
                })}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        )}
      </div>
    </div>
  );
}

export default SinglePost;
