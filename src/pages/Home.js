import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { Grid, Image, Transition } from "semantic-ui-react";
import PostCard from "../components/PostCard";
import { FETCH_POSTS_QUERY } from "../util/graphql";

function Home() {
  const { loading, data } = useQuery(FETCH_POSTS_QUERY);
  const { getPosts: posts } = data || [];
  if (posts) {
    //  console.log(posts);
  }
  return (
    <div>
      <Transition.Group duration={300}>
        <Grid columns={3} divided>
          <Grid.Row>
            <h1>Recent post</h1>
          </Grid.Row>

          <Grid.Row>{loading && <h2>loading post..</h2>}</Grid.Row>
        </Grid>

        <Grid>
          {!loading &&
            posts &&
            posts.map((post) => {
              return (
                <Grid.Column mobile={16} tablet={8} computer={4} key={post.id}>
                  <PostCard post={post} />
                </Grid.Column>
              );
            })}
        </Grid>
      </Transition.Group>
    </div>
  );
}
export default Home;
