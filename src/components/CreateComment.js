import React from "react";
import { Card, Form, TextArea, Button } from "semantic-ui-react";
import { useForm } from "../util/hook";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { FETCH_POSTS_QUERY, GET_POST_DETAILS } from "../util/graphql";

function CreateComment(props) {
  const { postId } = props.match.params;
  const { onSubmit, onChange, values } = useForm(createNewCommentOnPost, {
    body: "",
  });

  const [createNewComment, { loading }] = useMutation(CREATE_COMMENTS, {
    update(proxy, result) {
      //    console.log("isdsds", result);
      // result.data.createComment
      values.body = "";

      // const data = proxy.readQuery({
      //   query: GET_POST_DETAILS,
      //   variables: { postId },
      // });
      // console.log("My Data", data);
      //   result.data.createComment
      //   let oldComments = data.getPost.comments;
      // data.getPost.comments = [
      //   result.data.createComment,
      //   ...data.getPost.comments,
      // ];
      // proxy.writeQuery({ query: GET_POST_DETAILS, data });
    },
    onError(err) {
      console.log("error occured", err);
    },
    variables: {
      postId,
      body: values.body,
    },
  });

  function createNewCommentOnPost() {
    createNewComment();
  }

  return (
    <Card fluid>
      <Card.Content extra>
        <Form
          onSubmit={onSubmit}
          noValidate
          className={loading ? "loading" : ""}
        >
          <Form.Field
            control={TextArea}
            label="Comment"
            placeholder="Tell us more "
            value={values.body}
            name="body"
            onChange={onChange}
          />

          <Form.Field control={Button}>Submit</Form.Field>
        </Form>
      </Card.Content>
    </Card>
  );
}

const CREATE_COMMENTS = gql`
  mutation createComment($postId: ID!, $body: String!) {
    createComment(postId: $postId, body: $body) {
      id
      body
      username
      createdAt
      comments {
        id
        body
        username
      }
      likes {
        id
        username
        createdAt
      }
      commentCount
      likeCount
    }
  }
`;

export default CreateComment;
