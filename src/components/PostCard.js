import React, { useContext } from "react";
import { Card, Label, Image, Button, Icon } from "semantic-ui-react";
import moment from "moment";
import { Link } from "react-router-dom";
import { AuthContext } from "../context/auth";
import LikeButton from "./LikeButton";

import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { FETCH_POSTS_QUERY } from "../util/graphql";

function PostCard(props) {
  const {
    body,
    commentCount,
    comments,
    createdAt,
    id,
    likeCount,
    likes,
    username,
  } = props.post;

  const { user } = useContext(AuthContext);

  const commentOnPost = () => {
    console.log("Comment on post ");
  };

  const likeThePost = () => {
    console.log("like the post ");
  };

  const [deleteThisPost, { loading }] = useMutation(DELETE_POST, {
    update(proxy, result) {
      console.log(result);

      const data = proxy.readQuery({
        query: FETCH_POSTS_QUERY,
      });
      let allPostUpdated = { getPosts: [] };
      const latestPosts = data.getPosts.filter((post) => post.id != id);
      allPostUpdated.getPosts = [...latestPosts];
      proxy.writeQuery({ query: FETCH_POSTS_QUERY, data: allPostUpdated });
    },
    onError(err) {
      console.log(err.graphQLErrors[0]);
      if (err.graphQLErrors[0]) {
      }
    },
    variables: { postId: id },
  });

  const deletePost = () => {
    console.log("Deleteimg tje post ", id);
    deleteThisPost();
  };

  return (
    <div>
      <Card>
        <Card.Content as={Link} to={`/posts/${id}`}>
          <Image
            floated="right"
            size="mini"
            src="https://react.semantic-ui.com/images/avatar/large/jenny.jpg"
          />
          <Card.Header>{username}</Card.Header>
          <Card.Meta as={Link} to={`/posts/${id}`}>
            {moment(createdAt).fromNow()}
          </Card.Meta>
          <Card.Description>{body}</Card.Description>
        </Card.Content>

        <Card.Content extra>
          <LikeButton post={props.post} user={user} />

          <Button as="div" labelPosition="right" onClick={commentOnPost} basic>
            <Button color="blue">
              <Icon name="comments" />
            </Button>
            <Label basic color="blue" pointing="left">
              {commentCount}
            </Label>
          </Button>

          {user && user.username === username && (
            <Button as="div" color="red" onClick={deletePost} floated="right">
              <Icon name="trash" />
            </Button>
          )}
        </Card.Content>
      </Card>
    </div>
  );
}

const DELETE_POST = gql`
  mutation deletePost($postId: ID!) {
    deletePost(postId: $postId)
  }
`;

export default PostCard;
