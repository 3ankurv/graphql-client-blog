import React, { useState, useContext } from "react";
import { Form, Button } from "semantic-ui-react";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { useForm } from "../util/hook";
import { AuthContext } from "../context/auth";
import { FETCH_POSTS_QUERY } from "../util/graphql";

function CreatePost(props) {
  const [errors, setErrors] = useState([]);
  const context = useContext(AuthContext);
  const { onSubmit, onChange, values } = useForm(createNewPost, {
    body: "",
  });

  const [createMyPost, { loading }] = useMutation(CREATE_POST, {
    update(proxy, result) {
      //  console.log(result);

      const data = proxy.readQuery({
        query: FETCH_POSTS_QUERY,
      });
      // console.log(data, "===============>");

      data.getPosts = [result.data.createPost, ...data.getPosts];
      proxy.writeQuery({ query: FETCH_POSTS_QUERY, data });
      values.body = "";
      //  props.history.push("/");
    },
    onError(err) {
      //  console.log(err.graphQLErrors, "error hia");
      const { message } = err.graphQLErrors[0];
      if (message) {
        setErrors({ message });
      }
    },
    variables: values,
  });

  function createNewPost() {
    console.log("On Form Submit", values);
    createMyPost();
  }

  return (
    <div className="form-container">
      <Form
        name="login"
        onSubmit={onSubmit}
        noValidate
        className={loading ? "loading" : ""}
      >
        <h1>Add Post</h1>
        <Form.Input
          label="Post Content"
          placeholder="post content"
          name="body"
          error={errors?.body ? true : false}
          type="text"
          value={values.body}
          onChange={onChange}
        />

        <Button type="submit" primary>
          Login
        </Button>
      </Form>
      {errors && Object.keys(errors).length > 0 && (
        <div className="ui error message">
          {console.log(errors)}
          <ul className="list">
            {Object.values(errors).map((value) => {
              return <li key={value}>{value}</li>;
            })}
          </ul>
        </div>
      )}
    </div>
  );
}

const CREATE_POST = gql`
  mutation createPost($body: String!) {
    createPost(body: $body) {
      id
      body
      username
      createdAt
      comments {
        id
        body
        username
      }
      likes {
        id
        username
        createdAt
      }
      commentCount
      likeCount
    }
  }
`;

export default CreatePost;
