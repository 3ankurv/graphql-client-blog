import React, { useContext, useState } from "react";
import { Menu, Segment, Message } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { AuthContext } from "../context/auth";

function MenuBar() {
  const { user, logout } = useContext(AuthContext);
  const { pathname } = window.location;
  const path = pathname === "/" ? "home" : pathname.substr(1);
  const [activeItem, setActiveItem] = useState(path);

  const handleItemClick = (e, { name }) => setActiveItem(name);

  const menu = user ? (
    <Menu pointing secondary size="huge" color="brown">
      <Menu.Item name={user.username || "Home"} active as={Link} to="/" />

      <Menu.Item
        name="Create Post"
        active={activeItem === "Create Post"}
        onClick={handleItemClick}
        as={Link}
        to="/addPost"
      />

      <Menu.Menu position="right">
        <Menu.Item name="logout" onClick={logout} as={Link} />
      </Menu.Menu>
    </Menu>
  ) : (
    <Menu pointing secondary size="huge" color="brown">
      <Menu.Item
        name="home"
        active={activeItem === "home"}
        onClick={handleItemClick}
        as={Link}
        to="/"
      />

      {/* 
      <Menu.Item
        name="messages"
        active={activeItem === "messages"}
        onClick={handleItemClick}
      /> */}

      <Menu.Menu position="right">
        <Menu.Item
          name="login"
          active={activeItem === "login"}
          onClick={handleItemClick}
          as={Link}
          to="/login"
        />

        <Menu.Item
          name="register"
          active={activeItem === "register"}
          onClick={handleItemClick}
          as={Link}
          to="/register"
        />
      </Menu.Menu>
    </Menu>
  );
  console.log(activeItem);
  return (
    <>
      {menu}

      {/* <Message color="red">Red</Message> */}
    </>
  );
}
export default MenuBar;
