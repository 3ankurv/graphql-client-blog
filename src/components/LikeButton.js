import React, { useEffect, useState } from "react";
import { Card, Label, Image, Button, Icon } from "semantic-ui-react";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";

function LikeButton({ user, post: { likeCount = 0, id, likes } }) {
  const [liked, setLiked] = useState(false);
  // console.log("im he", likes, id, likeCount);
  useEffect(() => {
    if (user && likes.find((l) => l.username === user.username)) {
      setLiked(true);
    } else {
      setLiked(false);
    }
  }, [user, likes]);

  const [likeThisPost, { loading }] = useMutation(LIKE_POST, {
    // update(proxy, result) {
    //   console.log(result);
    // },
    onError(err) {
      //  console.log(err.graphQLErrors[0]);
      if (err.graphQLErrors[0]) {
      }
    },
    variables: { postId: id },
  });

  const likeThePost = () => {
    //  console.log("LIKEMR");
    likeThisPost();
  };

  return (
    <Button as="div" labelPosition="right" onClick={likeThePost} basic>
      <Button color="">
        {liked ? (
          <Icon name="heart" color="red" />
        ) : (
          <Icon name="heart" color="grey" />
        )}
      </Button>
      <Label basic color="red" pointing="left">
        {likeCount}
      </Label>
    </Button>
  );
}

const LIKE_POST = gql`
  mutation likePost($postId: ID!) {
    likePost(postId: $postId) {
      id
      body
      username
      createdAt
      comments {
        id
        body
        username
      }
      likes {
        id
        username
        createdAt
      }
      likeCount
      commentCount
    }
  }
`;

export default LikeButton;
