import React from "react";
import App from "./App";
import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createHttpLink } from "apollo-link-http";
import { ApolloProvider } from "@apollo/react-hooks";
import { setContext } from "apollo-link-context";

const setAuthorizationLink = setContext((request, previousContext) => {
  const token = window.localStorage.getItem("jwtToken");
  return {
    headers: { authorization: token ? `Bearer ${token}` : "" },
  };
});

const httpLink = createHttpLink({
  uri:
    process.env.NODE_ENV !== "production"
      ? "http://localhost:5000/graphql"
      : "https://blog-app-graphql.herokuapp.com/graphql",
});

const client = new ApolloClient({
  link: setAuthorizationLink.concat(httpLink),
  cache: new InMemoryCache(),
});

function MyApp() {
  return (
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  );
}

export default MyApp;
