import React, { useContext } from "react";
import "semantic-ui-css/semantic.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import MenuBar from "./components/MenuBar";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import { Container } from "semantic-ui-react";
import { AuthProvider } from "./context/auth";
import AuthRoute from "./util/AuthRoute";
import CreatePost from "./components/CreatePost";
import SinglePost from "./pages/SinglePost";
function App() {
  return (
    <div className="App">
      <AuthProvider>
        <Container>
          <Router>
            <MenuBar />

            <Route exact path="/" component={Home} />
            <AuthRoute exact path="/login" component={Login} />
            <AuthRoute exact path="/register" component={Register} />
            <Route exact path="/addPost" component={CreatePost} />
            <Route exact path="/posts/:postId" component={SinglePost} />
          </Router>
        </Container>
      </AuthProvider>
    </div>
  );
}

export default App;
